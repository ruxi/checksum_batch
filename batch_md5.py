#!/usr/bin/env python
"""
author: <lewis.r.liu@gmail.com>
date: Feb 4, 2015
"""
import os.path
import hashlib
def batch_md5(directory, filepattern='gz', md5pattern='md5', verbose = True):
    
    # get list of paths by pattern
    reference_md5_pathlist = [x for x in os.listdir(directory) if md5pattern in x] 
    filelist = [x for x in os.listdir(directory) if not('md5' in x) and (filepattern in x)]

    
    #dictionary with reference md5
    ref = {}
    for path in reference_md5_pathlist:
        f = open(path,'r')
        line = f.readline()  
        md5value = line.split()[0]
        filename = line.split()[1]
        ref[filename] = md5value
      
    # dictionary with checksum values from file
    check = {}
    for path in filelist:
        value = hashlib.md5(open(path,'rb').read()).hexdigest()
        check[path] = value

    # compare dictionaries
    summary = {}
    for key in [x for x in check.keys() if x in ref.keys()]:
        if check[key] == ref[key]:
            correct = True
        else:
            correct = False

        summary[key] = {'correct':correct, 
                        'md5sum_check':check[key],
                        'md5sum_ref  ':ref[key]
                        }
        if verbose:
            print(correct, key)

    return summary